const express = require("express")
const jogadorJson = require("./exer35.json")
const app = express()


app.get("/gerador", function (req, res) {
   
    const { nome, sobrenome, posicao, clube } = jogadorJson
    
    const name = getArrayRandomElement(nome)
    const surname = getArrayRandomElement(sobrenome)
    const age = Math.floor(Math.random() * (40 - 17 + 1) + 17)
    const position = getArrayRandomElement(posicao)
    const club = getArrayRandomElement(clube)

    const frase = name + " " + surname + " é um futebolista brasileiro de " + age + " anos que atua como " + position 
    + ". Atualmente defende o " + club + "."  

    res.send(frase)
}) 


app.listen(8080, function () {
    console.log("Conectado com sucesso!")
})


function getArrayRandomElement(vetor) {
        return vetor[Math.floor(Math.random() * vetor.length)];
}


function ramdom(vetor) {
    var Aleatorio  = getArrayRandomElement(0, 17, 40)
    return vetor[Aleatorio]
}