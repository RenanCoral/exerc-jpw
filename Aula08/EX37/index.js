const express = require('express');
const fs = require('fs');
const app = express();

app.use(express.json());

app.post("/jogador", (req, res) => {
    const body = req.body;

    const nomes = [], clubes = [];
    body.forEach(element => {
        if (Object.keys(element) == 'nome') {
            nomes.push(Object.values(element).toString());
        } else if (Object.keys(element) == 'clube') {
            clubes.push(Object.values(element).toString());
        }
    });

    const data = fs.readFileSync("./gerador.json", "utf8");
    const { nome, sobrenome, posicao, clube } = JSON.parse(data);
    if (nomes.length != 0) {
        nome.push(nomes.toString());
    }
    if (clubes.length != 0) {
        clube.push(clubes.toString());
    }
    const json = { nome, sobrenome, posicao, clube };

    fs.writeFileSync("./gerador.json", JSON.stringify(json), { encoding: 'utf8', flag: 'w' });

    res.json({
        message: "O arquivo foi atualizado com sucesso.",
        json: json
    });
})

app.listen(8080, () => {
    console.log("O servidor está online.")
})