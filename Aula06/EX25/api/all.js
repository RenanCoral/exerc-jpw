/*
    Lista de todos os pokemons
*/

var data = require('../data')

module.exports = function(req, res){

    var result = {}

    //Implementação
    result = data.pokemon
    
    //Retorno
    res.json(result)
}