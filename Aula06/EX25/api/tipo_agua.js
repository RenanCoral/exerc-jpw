/*
    Lista apenas os pokemons do tipo agua (type = water)
*/

var data = require('../data')

module.exports = function(req, res){

    var result = data.pokemon.filter((pokemon) =>{
		return pokemon.type.includes("Water"); 
	});
    //Implementação
    // :TODO
    
    //Retorno
    res.json(result)
}